<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Task List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
</head>
<body>
<jsp:include page="index.jsp"/>
<div class="container-fluid">
    <div class="my-2" style="text-align: center">
        <c:if test="${not empty tasks}">
            <table class="table table-striped table-sm table-responsive-md">
                <thead>
                <tr>
                    <th>User Id</th>
                    <th>Task Id</th>
                    <th>Project Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Status Type</th>
                    <th>Start Date</th>
                    <th>Finish Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="task" items="${tasks}">
                    <tr>
                        <td>
                            <input type="text" value="${task.user.id}" disabled>
                        </td>
                        <td>
                            <input type="text" value="${task.id}" disabled>
                        </td>
                        <td>
                            <input type="text" value="${task.project.id}" disabled>
                        </td>
                        <td>${task.name}</td>
                        <td>${task.description}</td>
                        <td>${task.statusType}</td>
                        <td>${task.startDate}</td>
                        <td>${task.finishDate}</td>
                        <c:url var="update" value="/task-update/${task.user.id}/${task.project.id}/${task.id}"/>
                        <c:url var="delete" value="/task-delete/${task.user.id}/${task.project.id}/${task.id}"/>
                        <td>
                            <a href="${update}">Update</a>
                            | <a href="${delete}" onclick="if (!(confirm('Are you sure?'))) return false">Delete</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>
    </div>
    <c:if test="${empty tasks}">
        <div class="container my-5">The task list is empty!</div>
    </c:if>
    <p class="my-5">
        <a href="/task-create" class="btn btn-primary">Add New Task</a>
    </p>
</div>
</body>
</html>