<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>User List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
</head>
<body>
<jsp:include page="index.jsp"/>
<div class="container-fluid">
    <div class="my-2" style="text-align: center">
        <c:if test="${not empty users}">
            <table class="table table-striped table-sm table-responsive-md">
                <thead>
                <tr>
                    <th>User Id</th>
                    <th>Login</th>
                    <th>Role Type</th>
                    <th>Email</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="user" items="${users}">
                    <tr>
                        <td>
                            <input value="${user.id}" disabled />
                        </td>
                        <td>${user.login}</td>
                        <td>${user.roleType}</td>
                        <td>${user.email}</td>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td>${user.phone}</td>
                        <c:url var="update" value="/user-update/${user.id}"/>
                        <c:url var="delete" value="/user-delete/${user.id}"/>
                        <td>
                            <a href="${update}">Update</a>
                            | <a href="${delete}" onclick="if (!(confirm('Are you sure?'))) return false">Delete</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>
    </div>
    <c:if test="${empty users}">
        <div class="container my-5">The user list is empty!</div>
    </c:if>
    <p class="my-5">
        <a href="/user-create" class="btn btn-primary">Add New User</a>
    </p>
</div>
</body>
</html>