package ru.kolevatykh.spring.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.User;
import ru.kolevatykh.spring.repository.IUserRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService extends AbstractService<User> {

    @Autowired
    @NotNull private IUserRepository userRepository;

    public UserService() {
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) throws Exception, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new UserNotFoundException();
        return userRepository.getOne(id);
    }

    @Nullable
    public User findOneByLogin(@Nullable final String login) throws Exception, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new UserNotFoundException();
        return userRepository.findOneByLogin(login);
    }

    @Override
    public void persist(@Nullable final User user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        userRepository.save(user);
    }

    @Override
    public void merge(@Nullable User user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        userRepository.save(user);
    }

    @Override
    public void remove(@Nullable String id) throws Exception, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new UserNotFoundException();
        @NotNull final User user = userRepository.getOne(id);
        userRepository.delete(user);
    }

    @Override
    public void removeAll() {
        userRepository.deleteAll();
    }
}
