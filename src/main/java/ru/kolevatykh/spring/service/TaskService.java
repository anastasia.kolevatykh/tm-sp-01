package ru.kolevatykh.spring.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.TaskNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.Task;
import ru.kolevatykh.spring.repository.ITaskRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TaskService extends AbstractProjectTaskService<Task> {

    @Autowired
    @NotNull private ITaskRepository taskRepository;

    public TaskService() {
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findAllByUserId(userId);
    }

    @NotNull
    public List<Task> findTasksWithProjectId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findTasksWithProjectId(userId);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id)
            throws Exception, UserNotFoundException, EmptyInputException {
        if (id == null || id.isEmpty()) throw new EmptyInputException("task");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findOneById(userId, id);
    }

    @NotNull
    @Override
    public List<Task> findOneByName(@Nullable final String userId, @Nullable final String name)
            throws Exception, UserNotFoundException, EmptyInputException {
        if (name == null || name.isEmpty()) throw new EmptyInputException("task");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public void persist(@Nullable final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.save(task);
    }

    public void merge(@Nullable final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.save(task);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id)
            throws Exception, UserNotFoundException, EmptyInputException {
        if (id == null || id.isEmpty()) throw new EmptyInputException("task");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        taskRepository.delete(task);
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        for (@NotNull final Task task : findAllByUserId(userId))
            taskRepository.delete(task);
    }

    @Override
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByCreateDate(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findAllSortedByCreateDate(userId);
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByStartDate(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findAllSortedByStartDate(userId);
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByFinishDate(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findAllSortedByFinishDate(userId);
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByStatus(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findAllSortedByStatus(userId);
    }

    @NotNull
    @Override
    public List<Task> findAllBySearch(@Nullable final String userId, @Nullable final String search) throws Exception {
        if (search == null || search.isEmpty()) throw new Exception("[The search query is empty.]");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findAllBySearch(userId, '%' + search + '%');
    }

    public void removeTasksWithProjectId(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        for (@NotNull final Task task : findTasksWithProjectId(userId))
            taskRepository.delete(task);
    }

    public void removeProjectTasks(@Nullable final String userId, @Nullable final String projectId)
            throws Exception, UserNotFoundException, EmptyInputException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        for (@NotNull final Task task : findTasksByProjectId(userId, projectId))
            taskRepository.delete(task);
    }

    @NotNull
    public List<Task> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId)
            throws Exception, UserNotFoundException, EmptyInputException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findTasksByProjectId(userId, projectId);
    }

    @NotNull
    public List<Task> findTasksWithoutProject(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return taskRepository.findTasksWithoutProject(userId);
    }
}
