package ru.kolevatykh.spring.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.ProjectNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.Project;
import ru.kolevatykh.spring.repository.IProjectRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ProjectService extends AbstractProjectTaskService<Project> {

    @Autowired
    @NotNull private IProjectRepository projectRepository;

    public ProjectService() {
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id)
            throws Exception, EmptyInputException, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return projectRepository.findOneById(userId, id);
    }

    @NotNull
    @Override
    public List<Project> findOneByName(@Nullable final String userId, @Nullable final String name)
            throws Exception, EmptyInputException, UserNotFoundException {
        if (name == null || name.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public void persist(@Nullable final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.save(project);
    }

    public void merge(@Nullable final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.save(project);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id)
            throws Exception, EmptyInputException, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.delete(project);
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        for (@NotNull final Project project : findAllByUserId(userId))
            projectRepository.delete(project);
    }

    @Override
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByCreateDate(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return projectRepository.findAllSortedByCreateDate(userId);
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByStartDate(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return projectRepository.findAllSortedByStartDate(userId);
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByFinishDate(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return projectRepository.findAllSortedByFinishDate(userId);
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByStatus(@Nullable final String userId)
            throws Exception, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return projectRepository.findAllSortedByStatus(userId);
    }

    @NotNull
    @Override
    public List<Project> findAllBySearch(@Nullable final String userId, @Nullable final String search)
            throws Exception {
        if (search == null || search.isEmpty()) throw new Exception("[The search query is empty.]");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return projectRepository.findAllBySearch(userId, '%' + search + '%');
    }
}
