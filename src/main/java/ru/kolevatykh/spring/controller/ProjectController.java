package ru.kolevatykh.spring.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kolevatykh.spring.enumerate.StatusType;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.ProjectNotFoundException;
import ru.kolevatykh.spring.exception.TaskNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.Project;
import ru.kolevatykh.spring.model.User;
import ru.kolevatykh.spring.service.ProjectService;
import ru.kolevatykh.spring.service.UserService;
import ru.kolevatykh.spring.util.DateFormatterUtil;

import java.util.Date;
import java.util.List;

@Controller
public class ProjectController {

    @NotNull
    private final UserService userService;

    @NotNull
    private final ProjectService projectService;

    @Autowired
    public ProjectController(
            @NotNull final UserService userService,
            @NotNull final ProjectService projectService
    ) {
        this.userService = userService;
        this.projectService = projectService;
    }

    @GetMapping("/project-list")
    public String findAll(@NotNull final Model model) throws Exception {
        @NotNull final List<Project> projects = projectService.findAll();
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping("/project-create")
    public String createProjectForm(@NotNull final Model model) throws Exception {
        return "project-create";
    }

    @PostMapping("/project-create")
    public String createProject(
            @ModelAttribute("user_id") @Nullable final String user_id,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("status_type") @Nullable final String status_type,
            @ModelAttribute("start_date") @Nullable final String start_date,
            @ModelAttribute("finish_date") @Nullable final String finish_date
    ) throws Exception, UserNotFoundException, ProjectNotFoundException {
        if (name == null) throw new EmptyInputException("project name");
        @NotNull final Project project = new Project(name, description, StatusType.valueOf(status_type));
        if (user_id == null || user_id.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = userService.findOneById(user_id);
        if (user == null) throw new UserNotFoundException();
        project.setUser(user);
        project.setStartDate(DateFormatterUtil.parseDate(start_date));
        project.setFinishDate(DateFormatterUtil.parseDate(finish_date));
        projectService.persist(project);
        return "redirect:/project-list";
    }

    @GetMapping("/project-delete/{user_id}/{id}")
    public String deleteProject(
            @PathVariable("user_id") @NotNull final String user_id,
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        projectService.remove(user_id, id);
        return "redirect:/project-list";
    }

    @GetMapping("/project-update/{user_id}/{id}")
    public String updateProjectForm(
            @PathVariable("user_id") @NotNull final String user_id,
            @PathVariable("id") @NotNull final String id,
            @NotNull final Model model
    ) throws Exception {
        @Nullable final Project project = projectService.findOneById(user_id, id);
        model.addAttribute("project", project);
        return "project-update";
    }

    @PostMapping("/project-update")
    public String updateProject(
            @ModelAttribute("user_id") @Nullable final String user_id,
            @ModelAttribute("id") @Nullable final String id,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("status_type") @Nullable final String status_type,
            @ModelAttribute("start_date") @Nullable final String start_date,
            @ModelAttribute("finish_date") @Nullable final String finish_date
    ) throws Exception, UserNotFoundException, ProjectNotFoundException {
        @Nullable final Project project = projectService.findOneById(user_id, id);
        if (project == null) throw new ProjectNotFoundException();
        if (user_id == null || user_id.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = userService.findOneById(user_id);
        if (user == null) throw new UserNotFoundException();
        project.setUser(user);
        if (name == null) throw new EmptyInputException("project name");
        project.setName(name);
        project.setDescription(description);
        project.setStatusType(StatusType.valueOf(status_type));
        project.setStartDate(DateFormatterUtil.parseDate(start_date));
        project.setFinishDate(DateFormatterUtil.parseDate(finish_date));
        projectService.merge(project);
        return "redirect:/project-list";
    }
}
