package ru.kolevatykh.spring.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kolevatykh.spring.enumerate.StatusType;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.ProjectNotFoundException;
import ru.kolevatykh.spring.exception.TaskNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.Task;
import ru.kolevatykh.spring.model.User;
import ru.kolevatykh.spring.service.ProjectService;
import ru.kolevatykh.spring.service.TaskService;
import ru.kolevatykh.spring.service.UserService;
import ru.kolevatykh.spring.util.DateFormatterUtil;

import java.util.Date;
import java.util.List;

@Controller
public class TaskController {

    @NotNull
    private final UserService userService;

    @NotNull
    private final ProjectService projectService;

    @NotNull
    private final TaskService taskService;

    @Autowired
    public TaskController(
            @NotNull final UserService userService,
            @NotNull final ProjectService projectService,
            @NotNull final TaskService taskService
    ) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @GetMapping("/task-list")
    public String findAll(@NotNull final Model model) throws Exception {
        @NotNull final List<Task> tasks = taskService.findAll();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task-create")
    public String createTask(@NotNull final Model model) throws Exception {
        return "task-create";
    }

    @PostMapping("/task-create")
    public String createTask(
            @ModelAttribute("user_id") @Nullable final String user_id,
            @ModelAttribute("project_id") @Nullable final String project_id,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("status_type") @NotNull final String status_type,
            @ModelAttribute("start_date") @Nullable final String start_date,
            @ModelAttribute("finish_date") @Nullable final String finish_date
    ) throws Exception, TaskNotFoundException {
        if (name == null) throw new EmptyInputException("task name");
        @NotNull final Task task = new Task(name, description, StatusType.valueOf(status_type));
        if (user_id == null || user_id.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = userService.findOneById(user_id);
        if (user == null) throw new UserNotFoundException();
        task.setUser(user);
        task.setProject(projectService.findOneById(user_id, project_id));
        task.setStartDate(DateFormatterUtil.parseDate(start_date));
        task.setFinishDate(DateFormatterUtil.parseDate(finish_date));
        taskService.persist(task);
        return "redirect:/task-list";
    }

    @GetMapping("/task-delete/{user_id}/{id}")
    public String deleteTask(
            @PathVariable("user_id") @NotNull final String user_id,
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        taskService.remove(user_id, id);
        return "redirect:/task-list";
    }

    @GetMapping("/task-update/{user_id}/{project_id}/{id}")
    public String updateTaskForm(
            @PathVariable("user_id") @NotNull final String user_id,
            @PathVariable("project_id") @Nullable final String project_id,
            @PathVariable("id") @NotNull final String id,
            @NotNull final Model model
    ) throws Exception {
        @Nullable final Task task = taskService.findOneById(user_id, id);
        model.addAttribute("task", task);
        return "task-update";
    }

    @PostMapping("/task-update")
    public String updateTask(
            @ModelAttribute("user_id") @Nullable final String user_id,
            @ModelAttribute("project_id") @Nullable final String project_id,
            @ModelAttribute("id") @Nullable final String id,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("status_type") @NotNull final String status_type,
            @ModelAttribute("start_date") @Nullable final String start_date,
            @ModelAttribute("finish_date") @Nullable final String finish_date
    ) throws Exception, UserNotFoundException, ProjectNotFoundException {
        @Nullable final Task task = taskService.findOneById(user_id, id);
        if (task == null) throw new TaskNotFoundException();
        if (user_id == null || user_id.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = userService.findOneById(user_id);
        if (user == null) throw new UserNotFoundException();
        if (project_id == null || project_id.isEmpty()) throw new ProjectNotFoundException();
        task.setProject(projectService.findOneById(user_id, project_id));
        if (name == null) throw new EmptyInputException("task name");
        task.setName(name);
        task.setDescription(description);
        task.setStatusType(StatusType.valueOf(status_type));
        task.setStartDate(DateFormatterUtil.parseDate(start_date));
        task.setFinishDate(DateFormatterUtil.parseDate(finish_date));
        taskService.merge(task);
        return "redirect:/task-list";
    }
}
