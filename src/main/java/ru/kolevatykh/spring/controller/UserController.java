package ru.kolevatykh.spring.controller;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kolevatykh.spring.enumerate.RoleType;
import ru.kolevatykh.spring.exception.ProjectNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.User;
import ru.kolevatykh.spring.service.UserService;
import ru.kolevatykh.spring.util.PasswordHashUtil;

import java.util.List;

@Controller
public class UserController {

    private Logger logger = Logger.getLogger(UserController.class);

    @NotNull
    private final UserService userService;

    @Autowired
    public UserController(@NotNull final UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/user-list")
    public String findAll(@NotNull final Model model) throws Exception {
        @NotNull final List<User> users = userService.findAll();
        model.addAttribute("users", users);
        return "user-list";
    }

    @GetMapping("/user-create")
    public String createUser(@NotNull final Model model) throws Exception {
        return "user-create";
    }

    @PostMapping("/user-create")
    public String createUser(
            @ModelAttribute("login") @NotNull final String login,
            @ModelAttribute("password") @NotNull final String password,
            @ModelAttribute("role_type") @NotNull final String role_type,
            @ModelAttribute("email") @Nullable final String email,
            @ModelAttribute("first_name") @Nullable final String first_name,
            @ModelAttribute("last_name") @Nullable final String last_name,
            @ModelAttribute("phone") @Nullable final String phone
    ) throws UserNotFoundException {
        @NotNull final User user =
                new User(login, PasswordHashUtil.getPasswordHash(password), RoleType.valueOf(role_type));
        user.setEmail(email);
        user.setFirstName(first_name);
        user.setLastName(last_name);
        user.setPhone(phone);
        userService.persist(user);
        return "redirect:/user-list";
    }

    @GetMapping("/user-delete/{id}")
    public String deleteUser(
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        userService.remove(id);
        return "redirect:/user-list";
    }

    @GetMapping("/user-update/{id}")
    public String updateUserForm(
            @PathVariable("id") @NotNull final String id,
            @NotNull final Model model
    ) throws Exception {
        @Nullable final User user = userService.findOneById(id);
        model.addAttribute("user", user);
        return "user-update";
    }

    @PostMapping("/user-update")
    public String updateUser(
            @ModelAttribute("id") @NotNull final String id,
            @ModelAttribute("login") @NotNull final String login,
            @ModelAttribute("password") @NotNull final String password,
            @ModelAttribute("role_type") @NotNull final String role_type,
            @ModelAttribute("email") @Nullable final String email,
            @ModelAttribute("first_name") @Nullable final String first_name,
            @ModelAttribute("last_name") @Nullable final String last_name,
            @ModelAttribute("phone") @Nullable final String phone
    ) throws Exception, UserNotFoundException, ProjectNotFoundException {
        @Nullable final User user = userService.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLogin(login);
        user.setPasswordHash(PasswordHashUtil.getPasswordHash(password));
        user.setRoleType(RoleType.valueOf(role_type));
        user.setEmail(email);
        user.setFirstName(first_name);
        user.setLastName(last_name);
        user.setPhone(phone);
        userService.merge(user);
        return "redirect:/user-list";
    }
}
